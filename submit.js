var $form = $("form#email-form");
var url = "https://script.google.com/macros/s/AKfycbw6__A9fDNlFaYFazgxCthT-D-f-RYUy8UQIuI_7JM8E0zjm23-/exec";

$("#submit-form").on("click", function(e) {
  e.preventDefault();
  var formData = $form.serializeObject();
  $("#invite-container").hide();
  $("#waiting").show();
  var jqxhr = $.ajax({
    url: url,
    method: "GET",
    dataType: "json",
    data: formData
  }).success(
    function(data) {
      $("#waiting").hide();
      $("#thank-you").show();
    }

  );
});

// dumps email in: https://docs.google.com/a/nilenso.com/spreadsheets/d/15M_NRo3eEel8O66x1dPQMbrLMzk7e1_Rh2dMD6H0h34/edit?usp=sharing
